spascii
=======

ASCII space greatness

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ spascii

Use --help/-h to view info on the arguments::

    $ spascii --help

Release Notes
-------------

:0.0.1:
    Project created
