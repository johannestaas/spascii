import math
from random import gauss
from .star_system import Star
from .util import calc_delta, minmax

try:
    from PIL import Image
except ImportError:
    pass


class Galaxy:

    def __init__(self, stars=None, size=100, num_stars=100, density=7):
        '''
        stars is the initial setup, if loading a save
        size is in thousands of light years (diameter), 100 being the milky way
        '''
        self.stars = stars or []
        self.density = minmax(density, 4, 12)
        self.num_stars = len(self.stars) or num_stars
        self.size = size
        if stars is None:
            self.build_stars()

    def build_stars(self):
        mid = self.size / 2
        density = mid / self.density
        for a in range(0, self.num_stars):
            rad = math.pi * 2 * (a / self.num_stars)
            d = minmax(gauss(mid / 2, density), 0, mid - 1)
            x = int(math.cos(rad) * d) + mid
            y = int(math.sin(rad) * d) + mid
            star = Star(pos=(x, y))
            self.stars.append(star)

    def show_galaxy_image(self):
        img = Image.new('RGB', (self.size, self.size), 'black')
        pixs = img.load()
        for star in self.stars:
            pixs[star.pos] = (255, 255, 255)
        img.show()
