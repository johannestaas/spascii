import traceback
from loggylog import Logger

LOG = Logger()
LOG.add_log('./spascii.log', level='debug')


def log_traceback(func):
    def new_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            LOG.error(traceback.format_exc())
            raise
    return new_func
