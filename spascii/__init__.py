'''
spascii

ASCII space greatness
'''

__title__ = 'spascii'
__version__ = '0.0.1'
__all__ = ('LOG',)
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2017 Johan Nestaas'

from ezcurses import curse

from .views import VIEWS
from .galaxy import Galaxy
from .log import LOG, log_traceback


@log_traceback
def new_game(scr):
    gb_setting = VIEWS['galaxy_builder'](scr).run()
    LOG.info('chose game settings: {!r}', gb_setting)
    fact = VIEWS['faction_chooser'](scr).run()
    LOG.info('chose faction {!r}', fact)
    galaxy = Galaxy(size=gb_setting.size, num_stars=gb_setting.size * 2,
                    density=gb_setting.density)
    VIEWS['galaxy_map'](scr).run(galaxy)


@log_traceback
def open_game(scr):
    pass


@curse
def main(scr):
    w, h = scr.max_size()
    opt = scr.get_menu_item([
        ('n', 'new game'),
        ('o', 'open game'),
        ('q', 'quit'),
    ], orig=(w // 2 - 6, h // 2 - 1), force=True)
    if opt == 0:
        new_game(scr)
    elif opt == 1:
        open_game(scr)
    elif opt == 2:
        return


if __name__ == '__main__':
    main()
