'''
'''

from ..setting import Setting
from . import View


class GalaxyBuilderView(View):

    def create_windows(self):
        self.layout = self._scr.new_layout()
        self.layout.add_row(6, [6, 6])
        self.layout.add_row(6, [6, 6])
        self.layout.draw()

    def run(self):
        self.create_windows()
        size_menu = self.draw_galaxy_size()
        rich_menu = self.draw_galaxy_richness()
        pirate_menu = self.draw_piracy()
        density_menu = self.draw_density()
        results = self._scr.multi_menu([
            size_menu,
            rich_menu,
            pirate_menu,
            density_menu,
        ])
        size = [20, 35, 50, 100, 200, 300, 500, 1000][results[0]]
        density = [4, 6, 7, 9, 11][results[3]]

        return Setting.new(
            'galaxy',
            size=size,
            richness=results[1],
            piracy=results[2],
            density=density,
        )

    def draw_galaxy_size(self):
        self.layout[0][0].write('Select galaxy size', (1, 1),
                                color=('red', 'black'))
        self.layout[0][0].refresh()
        return self.layout[0][0].new_menu([
                ('1', 'tiny'),
                ('2', 'small'),
                ('3', 'normal'),
                ('4', 'large'),
                ('5', 'huge'),
                ('6', 'grand'),
                ('7', 'epic'),
                ('8', 'carl sagan'),
            ],
            orig=(1, 2),
            selected=0,
        )

    def draw_galaxy_richness(self):
        self.layout[0][1].write('Select galaxy resource richness', (1, 1),
                                color=('red', 'black'))
        self.layout[0][1].refresh()
        return self.layout[0][1].new_menu([
                ('1', 'very poor'),
                ('2', 'poor'),
                ('3', 'normal'),
                ('4', 'rich'),
                ('5', 'super rich'),
                ('6', 'utopia'),
            ],
            orig=(1, 2),
            selected=2,
        )

    def draw_piracy(self):
        self.layout[1][0].write('Select threat of piracy', (1, 1),
                                color=('red', 'black'))
        self.layout[1][0].refresh()
        return self.layout[1][0].new_menu(
            [
                ('1', 'No pirates'),
                ('2', 'Few pirates'),
                ('3', 'Some piracy'),
                ('4', 'Heavy piracy'),
                ('5', 'Yo ho ho and a bottle of xenorum'),
            ],
            orig=(1, 2),
            selected=2,
        )

    def draw_density(self):
        self.layout[1][1].write('Select star distribution', (1, 1),
                                color=('red', 'black'))
        self.layout[1][1].refresh()
        return self.layout[1][1].new_menu(
            [
                ('1', 'Cosmic Void'),
                ('2', 'Sparse'),
                ('3', 'Normal'),
                ('4', 'Dense'),
                ('5', 'Crowded Galaxy'),
            ],
            orig=(1, 2),
            selected=2,
        )
