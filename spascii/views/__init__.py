'''
'''
from ..log import LOG


class View:

    def __init__(self, scr, *args, **kwargs):
        LOG.debug('creating view: {!r}', self.__class__.__name__)
        self._scr = scr


from .galaxy_builder import GalaxyBuilderView
from .galaxy_map import GalaxyMapView
from .faction_chooser import FactionChooserView


VIEWS = {
    'galaxy_builder': GalaxyBuilderView,
    'galaxy_map': GalaxyMapView,
    'faction_chooser': FactionChooserView,
}
