'''
'''

from ..faction import FACTIONS
from ..log import LOG
from ..util import chunk_text
from . import View


class FactionChooserView(View):

    FACTIONS = {}
    MENU_ITEMS = []

    def load_factions(self):
        for i, short_fact in enumerate(sorted(FACTIONS.items())):
            short_name, fact = short_fact
            name = fact.NAME
            desc = fact.DESCRIPTION
            self.FACTIONS[i] = (name, desc, fact)
        self.MENU_ITEMS = [
            (str(i), fact[0])
            for i, fact
            in sorted(self.FACTIONS.items())
        ]

    def create_windows(self):
        self.layout = self._scr.new_layout()
        self.layout.add_row(12, [6, 6])
        self.layout.draw()

    def run(self):
        self.create_windows()
        self.layout[0][0].write('Select faction', (1, 1),
                                color=('red', 'black'))
        self.layout[0][0].refresh()
        self.load_factions()
        menu = self.layout[0][0].new_menu(self.MENU_ITEMS, orig=(1, 2))
        while True:
            menu.draw()
            result = menu.get_item(force=True)
            name, desc, fact = self.FACTIONS[result]
            self.layout[0][1].clear()
            self.layout[0][1].write(name, (1, 1), color=('red', 'black'))
            width, _ = self.layout[0][1].max_size()
            width = min(width, 60)
            lines = chunk_text(desc, width)
            for i, line in enumerate(lines):
                self.layout[0][1].write(line, (1, 3 + i))
            self.layout[0][1].write(
                'Are you sure you want to choose {}?'.format(name),
                (1, i + 5),
                color=('red', 'black'),
            )
            self.layout[0][1].refresh()
            choice = self.layout[0][1].get_menu_item(
                [
                    ('y', 'Yes'),
                    ('n', 'No'),
                ],
                orig=(1, i + 6),
                force=True,
            )
            if choice == 0:
                break
        LOG.debug('user chose {!r}'.format(fact))
        return fact
