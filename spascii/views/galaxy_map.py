from . import View


class GalaxyMapView(View):

    def create_windows(self):
        self.layout = self._scr.new_layout()
        self.layout.add_row(12, [12])
        self.layout.draw()

    def run(self, galaxy):
        self.create_windows()
        win = self.layout[0][0]
        win.refresh()
        w, h = win.max_size()
        for star in galaxy.stars:
            x, y = star.pos
            x /= galaxy.size
            y /= galaxy.size
            x = int(x * w)
            y = int(y * h)
            win.write('*', (x, y), color='yellow')
        win.refresh()
        win.getkey()
