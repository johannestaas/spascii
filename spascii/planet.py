from enum import Enum
from random import gauss
from collections import namedtuple

from .namer import make_moon_name_i, MOON_NAME_GEN
from .util import choice_dist, normal


GAS_MOON_COEF = 0.003
MOON_COEF = 0.05
MOON_VARIANCE = 0.5
GAS_MOON_MASS_COEF = 0.01

# 1AU = 149.6e6KM
EARTH_DIST = 149.6
# 1M+ = 5.972e24KG
EARTH_MASS = 5.972

ProtoBody = namedtuple('ProtoBody', [
    'name', 'mass_g', 'moon_g', 'prob_f', 'moon_prob_f',
])


class BodyType(Enum):
    rock = ProtoBody(
        'rock',
        # mass mean and stddev
        (1.0, 0.5),
        # num of moons mean and stddev
        (1.0, 0.5),
        # distance probabilistic function
        lambda d, t: normal(t, 0, 200),
        # probability it's a moon within a range of mass
        lambda t: normal(t, 0, 200),
    )
    volcanic = ProtoBody(
        'volcanic',
        (1.0, 0.5),
        (1.0, 0.5),
        lambda d, t: normal(t, 300, 300) if t >= 32 else 0,
        lambda t: normal(t, 300, 300) if t >= 32 else 0,
    )
    continental = ProtoBody(
        'continental',
        (6.0, 1.0),
        (1.0, 0.3),
        lambda d, t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
        lambda t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
    )
    desert = ProtoBody(
        'desert',
        (5.0, 1.5),
        (1.0, 0.3),
        lambda d, t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
        lambda t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
    )
    jungle = ProtoBody(
        'jungle',
        (7.0, 1.5),
        (1.0, 0.3),
        lambda d, t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
        lambda t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
    )
    ocean = ProtoBody(
        'ocean',
        (8.0, 2.0),
        (1.5, 0.5),
        lambda d, t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
        lambda t: normal(t, -15, 10) * 5 if -32 < t < 32 else 0,
    )
    ice = ProtoBody(
        'ice',
        (1.0, 0.5),
        (1.0, 0.5),
        lambda d, t: normal(t, -300, 300) if t <= -32 else 0,
        lambda t: normal(t, -300, 300) if t <= -32 else 0,
    )
    gas_giant = ProtoBody(
        'gas_giant',
        (1500.0, 300.0),
        (5.0, 2.0),
        lambda d, t: normal(d, 1000, 400) if d > 500 else 0,
        lambda x: 0,
    )
    ice_giant = ProtoBody(
        'ice_giant',
        (100.0, 20.0),
        (4.0, 1.5),
        lambda d, t: normal(d, 3000, 1000) if d > 1000 else 0,
        lambda x: 0,
    )

    @classmethod
    def habitable_types(cls):
        return (cls.continental, cls.desert, cls.jungle, cls.ocean)

    @classmethod
    def type_probability(cls, distance, temp):
        dist = {
            type.value.name: type.value.prob_f(distance, temp)
            for type in cls
        }
        return BodyType[choice_dist(dist)]

    @classmethod
    def moon_type_probability(cls, temp):
        dist = {
            type.value.name: type.value.moon_prob_f(temp)
            for type in cls
        }
        return BodyType[choice_dist(dist)]


class Body:

    def repr_mass(self):
        return '{:.3f}M+'.format(self.mass / EARTH_MASS)

    def repr_dist(self):
        return '{:.3f}AU'.format(self.distance / EARTH_DIST)

    def __init__(self, name=None, mass=None, type=None, distance=None,
                 temp=None, parent=None, **kwargs):
        self.name = name
        self.type = type
        self.distance = distance
        self.mass = mass or max(gauss(*self.type.value.mass_g), 0.012)
        self.temp = temp
        self.parent = parent


class Moon(Body):

    def __repr__(self):
        return '{}Moon {} [{}, {}C]'.format(
            self.type.value.name.title().replace('_', ''),
            self.name,
            self.repr_mass(),
            int(self.temp),
        )


class Planet(Body):

    def __repr__(self):
        return '{}Planet {} [{}, {}C, {}, {} moons]'.format(
            self.type.value.name.title().replace('_', ''),
            self.name,
            self.repr_mass(),
            int(self.temp),
            self.repr_dist(),
            len(self.moons),
        )

    def __init__(self, moons=None, **kwargs):
        super().__init__(**kwargs)
        self.moons = moons or []
        if moons is None:
            num_moons = round(gauss(*self.type.value.moon_g))
            for i in range(num_moons):
                self.moons.append(self.new_moon())

    def new_moon(self):
        if self.type.name != 'gas':
            coef = self.mass * MOON_COEF
        else:
            coef = min(self.mass * GAS_MOON_COEF, 6.0)
        while True:
            mass = max(gauss(coef, coef * MOON_VARIANCE), 0.001)
            if mass < self.mass:
                break
        temp = self.temp + gauss(0, 50)
        type = BodyType.moon_type_probability(temp)
        if type in BodyType.habitable_types():
            name = MOON_NAME_GEN()
        else:
            name = make_moon_name_i(self.name, len(self.moons))
        return Moon(name=name, mass=mass, type=type, parent=self,
                    temp=temp)
