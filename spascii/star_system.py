'''
'''
import math
from random import gauss, uniform

from .namer import make_planet_name_i, PLANET_NAME_GEN, STAR_NAME_GEN
from .planet import Planet, BodyType, EARTH_DIST
# from .util import minmax
# from .setting import Setting

STEFAN_BOLTZMANN = 5.670373 * 10**-8
# units of 10^30 KG
# other main sequence stars can range from 1/10th to 200x as massive
SUN_MASS = 1.989
# units of 10^26 W
SUN_LUM = 3.846


def calc_planet_temp(lum=0, alb=0, dist=0):
    '''
    Returns temp in Celsius with that last Kelvin conversion
    '''
    return (
        (lum * (1 - alb)) /
        (16 * math.pi * dist**2 * STEFAN_BOLTZMANN)
    )**(1/4) - 273.15


def calc_luminosity(mass):
    '''
    Close enough.
    https://en.wikipedia.org/wiki/Main_sequence#Luminosity-color_variation
    '''
    return (mass / SUN_MASS)**3.5 * SUN_LUM


def rand_solar_mass():
    return max(gauss(1.0, 0.05), 0.75) * SUN_MASS


SYSTEM_DIST_CHOICES = [
    0.1, 0.3, 0.5, 0.7, 0.8, 1.0, 1.2, 1.3, 1.5, 3.5, 5.0, 9.0, 18.0, 25.0,
    35.0, 50.0,
]


class Star:

    def repr_mass(self):
        return '{:.3f}M'.format(self.mass / SUN_MASS)

    def repr_lumin(self):
        return '{:.3f}L'.format(self.lumin / SUN_LUM)

    def __repr__(self):
        return 'Star {!r} [{}, {}, {} planets]'.format(
            self.name, self.repr_mass(), self.repr_lumin(), len(self.planets),
        )

    def __init__(self, name=None, mass=None, planets=None, pos=None):
        self.name = name or STAR_NAME_GEN()
        self.mass = mass or rand_solar_mass()
        self.lumin = calc_luminosity(self.mass)
        self.pos = pos
        self.planets = planets or []
        if planets is None:
            self.spawn_planets()

    def spawn_planets(self):
        # Distance in 10^6 KM
        # Earth is 149.6e6 KM

        chance = 0.3

        for d in SYSTEM_DIST_CHOICES:
            dist = gauss(d, 0.05 * d) * EARTH_DIST
            temp = calc_planet_temp(
                lum=self.lumin * 10**26,
                dist=dist * 10**9,
                # alb=minmax(gauss(0.5, 0.2), 0.1, 0.9),
                alb=0.3,
            )
            type = BodyType.type_probability(dist, temp)
            if type in BodyType.habitable_types():
                # due to atmosphere + greenhouse effect
                temp += 32
                name = PLANET_NAME_GEN()
                chance /= 3
            else:
                name = make_planet_name_i(self.name, len(self.planets))
                rnd = uniform(0, 1)
                if rnd < chance:
                    chance /= 3
                else:
                    chance *= 1.5
                    continue
            planet = Planet(
                name=name,
                type=type,
                distance=dist,
                temp=temp,
                parent=self,
            )
            self.planets.append(planet)

    def output(self):
        print(repr(self))
        for planet in self.planets:
            print(' - {!r}'.format(planet))
            for moon in planet.moons:
                print('   - {!r}'.format(moon))
