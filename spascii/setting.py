from collections import namedtuple


class Setting:

    galaxy = namedtuple(
        'galaxy', ['size', 'richness', 'piracy', 'density'])(
            size=0, richness=2, piracy=2, density=2,
    )

    @classmethod
    def new(cls, name, **kwargs):
        nt = namedtuple(name, sorted(kwargs.keys()))
        val = nt(**kwargs)
        setattr(cls, name, val)
        return val
