import os
import random
from namerator import name_generator

STAR_NAMES = os.path.join(
    os.path.dirname(__file__), 'data', 'star_names.txt'
)
PLANET_NAMES = os.path.join(
    os.path.dirname(__file__), 'data', 'planet_names.txt'
)
STARGEN = name_generator(STAR_NAMES)
PLANETGEN = name_generator(PLANET_NAMES)
PICKED_NAMES = set()

SUFFIXS = [
    'majoris', 'minoris', 'borealis', 'australis', 'prior', 'posterior',
    'primus', 'secundus', 'tertius',
]
PREFIXS = [
    'alpha', 'beta', 'gamma', 'delta', 'epsilon', 'zeta', 'eta', 'theta',
    'iota', 'kappa', 'lambda', 'mu', 'nu', 'xi', 'omnicron', 'pi', 'rho',
    'sigma', 'tau', 'upsilon', 'phi', 'chi', 'psi', 'omega',
]
ROMAN_NUMERAL = [
    'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII',
    'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX',
]
ALPHANUM = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'


def alpha_char():
    return random.choice(ALPHANUM[:26])


def num_char():
    return str(random.randint(0, 9))


def char():
    return random.choice(ALPHANUM)


def star_name_generator():

    def star_name():
        while True:
            name = STARGEN()
            if random.randint(0, 4) == 0:
                suffix = random.choice(SUFFIXS)
                name = '{} {}'.format(name, suffix)
            if random.randint(0, 4) == 0:
                prefix = random.choice(PREFIXS)
                name = '{} {}'.format(prefix, name)
            name = name.title()
            if name not in PICKED_NAMES:
                PICKED_NAMES.add(name)
                return name

    return star_name


def planet_name_generator():

    def planet_name():
        while True:
            name = PLANETGEN()
            name = name.title()
            name = '{}-{}{}{}'.format(
                name, alpha_char(), num_char(), num_char(),
            )
            if name not in PICKED_NAMES:
                PICKED_NAMES.add(name)
                return name

    return planet_name


def moon_name_generator():

    def moon_name():
        while True:
            name = PLANETGEN()
            prefix = random.choice(PREFIXS)
            name = '{} {}'.format(name, prefix).title()
            if name not in PICKED_NAMES:
                PICKED_NAMES.add(name)
                return name

    return moon_name


def make_planet_name_i(star_name, i):
    return '{} {}'.format(star_name, ROMAN_NUMERAL[i])


def make_moon_name_i(planet_name, i):
    return '{}-{}'.format(planet_name, PREFIXS[i].title())


PLANET_NAME_GEN = planet_name_generator()
STAR_NAME_GEN = star_name_generator()
MOON_NAME_GEN = moon_name_generator()
