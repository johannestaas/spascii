'''
'''
from . import Faction


class HumanFaction(Faction):
    NAME = 'Federacy of Earth'
    DESCRIPTION = (
        'After World War IV, the humans finally decided to end the killing of '
        'one another and join forces as a single unit, spreading democracy to '
        'every end of the planet. Each continent democratically elected a '
        'leader and representatives and formed the council to defend Earth '
        'from hunger, poverty... and any future threats yet to be determined. '
        'In light of the discovery of the Superluminal Engine, those threats '
        'are now being discovered.'
    )
