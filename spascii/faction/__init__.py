'''
'''


class Faction:

    NAME = None
    DESCRIPTION = None
    ORIGIN = None


from .human import HumanFaction
from .salamander import SalamanderFaction

FACTIONS = {
    'human': HumanFaction,
    'salamander': SalamanderFaction,
}
