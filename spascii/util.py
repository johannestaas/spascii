'''
utility functions
'''
import math
import random

ROOT2 = math.sqrt(2)


def normal(x, mu=0.0, sigma=1.0):
    return (
        (
            math.e**(
                (-((x - mu) / sigma)**2) / 2
            )
        ) /
        (ROOT2 * math.pi)
    )


def chunk_text(s, width):
    words = s.split()
    lines = []
    line = ''
    for word in words:
        maybe = '{} {}'.format(line, word)
        if len(maybe) > width:
            lines.append(line)
            line = word
            continue
        else:
            line = maybe
    lines.append(line)
    return lines


def choice_dist(dist_map):
    s = 0
    choices = []
    for key, dist in sorted(dist_map.items()):
        s += dist
        choices.append((key, s))
    r = random.uniform(0, s)
    for key, total in choices[:-1]:
        if r < total:
            return key
    return choices[-1][0]


def minmax(val, mn, mx):
    return max(min(val, mx), mn)


def calc_delta(pos1, pos2):
    w = (pos1[0] - pos2[0])**2
    h = (pos1[1] - pos2[1])**2
    return math.sqrt(w + h)
