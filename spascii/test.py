from collections import Counter


def cmd_galaxy(size, num, density):
    from .galaxy import Galaxy
    g = Galaxy(size=size, num_stars=num, density=density)
    g.show_galaxy_image()


def cmd_create(n):
    from .star_system import Star

    for i in range(n):
        Star().output()


def cmd_ctr(n):
    from pprint import pprint
    from datetime import datetime
    from .star_system import Star

    pctr = Counter()
    mctr = Counter()

    last = datetime.now()
    for i in range(n):
        s = Star()
        for p in s.planets:
            pctr.update([p.type.name])
            for m in p.moons:
                mctr.update([m.type.name])
        if (datetime.now() - last).seconds >= 5:
            last = datetime.now()
            print('Finished {}/{} ({:.1%})'.format(i + 1, n, (i + 1) / n))

    print('PLANETS')
    pprint(pctr.most_common())
    print('\nMOONS')
    pprint(mctr.most_common())


def main():
    import sys
    import argparse
    parser = argparse.ArgumentParser()
    subs = parser.add_subparsers(dest='cmd')

    p = subs.add_parser('stars')
    p.add_argument('-n', type=int, default=20)
    p.add_argument('--ctr', action='store_true')

    p = subs.add_parser('galaxy')
    p.add_argument('--size', '-s', type=int, default=100)
    p.add_argument('--num', '-n', type=int, default=100)
    p.add_argument('--density', '-d', type=int, default=7)

    args = parser.parse_args()

    if args.cmd == 'stars':
        if args.ctr:
            cmd_ctr(args.n)
        else:
            cmd_create(args.n)
    elif args.cmd == 'galaxy':
        cmd_galaxy(args.size, args.num, args.density)
    else:
        parser.print_usage()
        sys.exit(1)
