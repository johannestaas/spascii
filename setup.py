import os
from setuptools import setup

# spascii
# ASCII space greatness


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="spascii",
    version="0.0.1",
    description="ASCII space greatness",
    author="Johan Nestaas",
    author_email="johannestaas@gmail.com",
    license="GPLv3+",
    keywords="",
    url="https://www.bitbucket.org/johannestaas/spascii",
    packages=['spascii'],
    package_dir={'spascii': 'spascii'},
    long_description=read('README.rst'),
    classifiers=[
        # 'Development Status :: 1 - Planning',
        # 'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        # 'Development Status :: 4 - Beta',
        # 'Development Status :: 5 - Production/Stable',
        # 'Development Status :: 6 - Mature',
        # 'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later '
        '(GPLv3+)',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
    ],
    install_requires=[
        'ezcurses',
        'loggylog',
        'namerator',
    ],
    entry_points={
        'console_scripts': [
            'spascii=spascii:main',
            'spascii-test=spascii.test:main',
        ],
    },
    # If you get errors running setup.py install:
    # zip_safe=False,
    #
    # For including non-python files:
    package_data={
        'spascii': ['data/*.txt'],
    },
    include_package_data=True,
)
